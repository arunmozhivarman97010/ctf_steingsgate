#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#define FLAG_LENGTH 32 

// Function to apply bit manipulation (XOR) on a string
void manipulate_string(char *str) {
    while (*str) {
        *str ^= 0x55;  // XOR each character with 0x55
        str++;
    }
}

// Function to generate a pseudo-random flag based on a seed
void generate_flag(char *flag, uint32_t seed) {
    srand(seed);
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for (size_t i = 0; i < FLAG_LENGTH; ++i) {
        flag[i] = charset[rand() % (sizeof(charset) - 1)];
    }
    flag[FLAG_LENGTH] = '\0';  // Null-terminate the flag
}

int main() {
    char input[100];
    char flag[FLAG_LENGTH + 1];
    uint32_t seed = 12345;  // Seed for flag generation

    // Generate the flag
    generate_flag(flag, seed);

    // Correct input string "reverse_me" XORed with 0x55
    char correct_input[] = { 'r' ^ 0x55, 'e' ^ 0x55, 'v' ^ 0x55, 'e' ^ 0x55, 'r' ^ 0x55, 's' ^ 0x55, 'e' ^ 0x55, '_' ^ 0x55, 'm' ^ 0x55, 'e' ^ 0x55, '\0' };

    printf("Enter msg for the past: ");
    fgets(input, sizeof(input), stdin);
    input[strcspn(input, "\n")] = '\0';  // Remove newline character

    // XOR the user input for comparison
    manipulate_string(input);

    // Compare the manipulated user input with the correct input
    int match = 1;
    for (size_t i = 0; i < strlen(correct_input); ++i) {
        if (input[i] != correct_input[i]) {
            match = 0;
            break;
        }
    }

    if (match) {
        printf("Congratulations! The flag is: %s\n", flag);
    } else {
        printf("MeSsAgE SeNt!\n");
    }

    return 0;
}

