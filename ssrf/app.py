from flask import Flask, render_template, send_from_directory, abort, current_app, jsonify, request, Response, redirect
from collections import OrderedDict
import os
import json

app = Flask(__name__)

flag = "CTF{ssrf_admin_bypass}"
blacklist = ['localhost', '127.0.0.1', '::1']

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/static/assets/img/<img_id>')
def img(img_id):
    img = {
        '201': 'folio-1.jpg',
        '202': 'folio-2.jpg',
        '203': 'folio-4.jpg',
        '204': 'folio-3.jpg',
        
        #leak
        '205': 'Z-program1.png',
        '206': 'Z-program2.png',
        '207': 'cristinadead.png',
        '208': 'Mayurideath.png',
        '209': 'rounders.jpg',
        
        '211': 'sern-logo.png',             
        '212': 'quantumphysics.jpg',
        '213': 'quantum-teleportation1.png',
        '214': 'largehadroncollider.webp',
        '215': 'lhc.webp',
        '216': 'quantummicrowave.webp',

    }
    
    filename = img.get(img_id)
    if filename:
        directory = current_app.root_path + '/static/assets/img'
        return send_from_directory(directory, filename)
    else:
        abort(404)

@app.route('/static/assets/documents/<doc_id>')
def documents(doc_id):
    documents = {
        '401': 'aqm-23.pdf',
        '402': 'aqm-24.pdf',
        '403': 'timemachine.pdf'
    }
    
    filename = documents.get(doc_id)
    
    if filename:
        directory = current_app.root_path + '/static/assets/documents'
        return send_from_directory(directory, filename)
    else:
        abort(404)
    
@app.route('/static/assets/users/<user_id>')
def user(user_id):
    user = {
        '301': 'nakabachi.jpg',
        '302': 'avatar-1.jpg',
        '303': 'maho1.png',
        '304': 'alexis.png',
        '305': 'hawking1.png',
        '306': 'Moeka_Kiryuu.png',
        '307': 'braun.png'
    }
    
    filename = user.get(user_id)
    if filename:
        directory = current_app.root_path + '/static/assets/users'
        return send_from_directory(directory, filename)
    else:
        abort(404)


@app.route('/api/put-email', methods=['PUT'])
def store_email():
    email_data = request.json
    if not email_data or not all(key in email_data for key in ("user_id", "from", "to", "subject", "body")):
        return "Invalid email data", 400
    
    email_file_path = os.path.join(current_app.root_path, 'static', 'email.json')
    
    if not os.path.exists(email_file_path):
        with open(email_file_path, 'w') as f:
            json.dump({"conversations": []}, f)

    with open(email_file_path, 'r+') as f:
        data = json.load(f)
        user_conversation = next((conv for conv in data["conversations"] if conv["user_id"] == email_data["user_id"]), None)
        email_entry = {
            "from": email_data["from"],
            "to": email_data["to"],
            "subject": email_data["subject"],
            "body": email_data["body"]
        }
        if user_conversation:
            user_conversation["emails"].append(email_entry)
        else:
            data["conversations"].append({
                "user_id": email_data["user_id"],
                "user_name": users.get(email_data["user_id"], "Unknown"),
                "emails": [email_entry]
            })
        f.seek(0)
        json.dump(data, f, indent=4)
        f.truncate()

    return "Email stored successfully", 200


@app.route('/api/get-email/<user_id>')
def retrieve_emails(user_id):
    email_file_path = os.path.join(current_app.root_path, 'static', 'email.json')
    
    if not os.path.exists(email_file_path):
        abort(404)
    
    with open(email_file_path, 'r') as f:
        data = json.load(f)
        user_conversation = next((conv for conv in data["conversations"] if conv["user_id"] == user_id), None)
        if user_conversation:
            emails_ordered = [
                OrderedDict([
                    ("from", email["from"]),
                    ("to", email["to"]),
                    ("subject", email["subject"]),
                    ("body", email["body"])
                ])
                for email in user_conversation["emails"]
            ]
            response = OrderedDict([
                ("user_name", user_conversation["user_name"]),
                ("emails", emails_ordered)
            ])
            response_json = json.dumps(response, indent=4)
            return Response(response_json, mimetype='application/json')
        else:
            abort(404)


@app.route('/logout')
def logout():
    session.pop('admin', None)
    return "Logged out. Go back to /login to try again."


@app.route('/admin_panel')
def admin_panel():
    if 'admin' in session:
        return '''
            Welcome to the Admin Panel. You can control the time machine at /control.
            <br><br>
            <form action="/control" method="post">
                <input type="hidden" name="action" value="start">
                <input type="submit" value="Start Time Machine">
            </form>
            <form action="/control" method="post">
                <input type="hidden" name="action" value="stop">
                <input type="submit" value="Stop Time Machine">
            </form>
            <form action="/control" method="post">
                <input type="hidden" name="action" value="self_destruct">
                <input type="submit" value="Self Destruct">
            </form>
            <br><br>
            <a href="/flag">Get the Flag</a>
        '''
    else:
        return "Unauthorized. Please login at /login.", 401


@app.route('/control', methods=['POST'])
def control():
    if 'admin' not in session:
        return "Unauthorized. Please login at /login.", 401

    action = request.form.get('action')
    if action == 'self_destruct':
        return f"Time machine self-destructed! The flag is: {flag}", 200
    return f"Action '{action}' performed.", 200


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        if username == admin_credentials['username'] and password == admin_credentials['password']:
            session['admin'] = True
            return redirect(url_for('admin_panel'))
        else:
            return "Invalid credentials. Hint: Try 'admin' for both username and password.", 401
    return '''
        <form method="post">
            Username: <input type="text" name="username"><br>
            Password: <input type="password" name="password"><br>
            <input type="submit" value="Login">
        </form>
    '''


@app.route('/admin')
def admin():
    action = request.args.get('action', '')
    if action == "self_destruct":
        return "Time machine self-destructed! Timeline secured! \nTake this: CTF{ssrf_admin_bypass}"
    return """
    <h1>Admin Page</h1>
    <ul>
        <li><a href="/admin?action=start">Start Time Machine</a></li>
        <li><a href="/admin?action=stop">Stop Time Machine</a></li>
        <li><a href="/admin?action=self_destruct">Self Destruct</a></li>
    </ul>
    """

@app.route('/project/<int:project_id>')
def project(project_id):
    return f'''
        <h1>Project {project_id}</h1>
        <h4>Status: Active</h4>
        <h4>You have arrived at the wrong page... </h4>
        <h4>Webpage Under Construction!</h4>

        <form action="/project/status" method="post">
            <input type="hidden" name="statusApi" value="/project/status/check?projectId={project_id}">
            <input type="submit" value="Check Status">
        </form>
        <a href="/project/nextProject?currentProjectId={project_id}&path=/project?projectId={project_id+1}">Next Project</a>
    '''

@app.route('/project/nextProject')
def next_project():
    path = request.args.get('path')
    if path:
        return redirect(path)
    return "No path provided.", 400

@app.route('/project/status', methods=['POST'])
def status():
    status_api = request.form.get('statusApi')
    if any(bl in status_api for bl in blacklist):
        return "URL is blacklisted! Hint: Try accessing internal URLs through other means.", 403
    
    try:
        response = request.get(f'http://localhost:5000{status_api}')
        if response.status_code == 200 and 'Welcome to the Admin Panel' in response.text:
            session['admin'] = True
            return redirect(url_for('admin_panel'))
        return response.content, response.status_code
    except request.RequestException as e:
        return str(e), 500


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
