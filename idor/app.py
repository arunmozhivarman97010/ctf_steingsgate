from flask import Flask, render_template, send_from_directory, abort, current_app, jsonify, request, Response
from collections import OrderedDict
import os
import json

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/static/assets/img/<img_id>')
def img(img_id):
    img = {
        '201': 'folio-1.jpg',
        '202': 'folio-2.jpg',
        '203': 'folio-4.jpg',
        '204': 'folio-3.jpg',
        
        #leak
        '205': 'Z-program1.png',
        '206': 'Z-program2.png',
        '207': 'cristinadead.png',
        '208': 'Mayurideath.png',
        '209': 'rounders.jpg',
        
        '211': 'sern-logo.png',             
        '212': 'quantumphysics.jpg',
        '213': 'quantum-teleportation1.png',
        '214': 'largehadroncollider.webp',
        '215': 'lhc.webp',
        '216': 'quantummicrowave.webp',

    }
    
    filename = img.get(img_id)
    if filename:
        directory = current_app.root_path + '/static/assets/img'
        return send_from_directory(directory, filename)
    else:
        abort(404)

@app.route('/static/assets/documents/<doc_id>')
def documents(doc_id):
    documents = {
        '401': 'aqm-23.pdf',
        '402': 'aqm-24.pdf',
        '403': 'timemachine.pdf'
    }
    
    filename = documents.get(doc_id)
    
    if filename:
        directory = current_app.root_path + '/static/assets/documents'
        return send_from_directory(directory, filename)
    else:
        abort(404)
    
@app.route('/static/assets/users/<user_id>')
def user(user_id):
    user = {
        '301': 'nakabachi.jpg',
        '302': 'avatar-1.jpg',
        '303': 'maho1.png',
        '304': 'alexis.png',
        '305': 'hawking1.png',
        '306': 'Moeka_Kiryuu.png',
        '307': 'braun.png'
    }
    
    filename = user.get(user_id)
    if filename:
        directory = current_app.root_path + '/static/assets/users'
        return send_from_directory(directory, filename)
    else:
        abort(404)



@app.route('/api/put-email', methods=['PUT'])
def store_email():
    email_data = request.json
    if not email_data or not all(key in email_data for key in ("user_id", "from", "to", "subject", "body")):
        return "Invalid email data", 400
    
    email_file_path = os.path.join(current_app.root_path, 'static', 'email.json')
    
    if not os.path.exists(email_file_path):
        with open(email_file_path, 'w') as f:
            json.dump({"conversations": []}, f)

    with open(email_file_path, 'r+') as f:
        data = json.load(f)
        user_conversation = next((conv for conv in data["conversations"] if conv["user_id"] == email_data["user_id"]), None)
        email_entry = {
            "from": email_data["from"],
            "to": email_data["to"],
            "subject": email_data["subject"],
            "body": email_data["body"]
        }
        if user_conversation:
            user_conversation["emails"].append(email_entry)
        else:
            data["conversations"].append({
                "user_id": email_data["user_id"],
                "user_name": users.get(email_data["user_id"], "Unknown"),
                "emails": [email_entry]
            })
        f.seek(0)
        json.dump(data, f, indent=4)
        f.truncate()

    return "Email stored successfully", 200


@app.route('/api/get-email/<user_id>')
def retrieve_emails(user_id):
    email_file_path = os.path.join(current_app.root_path, 'static', 'email.json')
    
    if not os.path.exists(email_file_path):
        abort(404)
    
    with open(email_file_path, 'r') as f:
        data = json.load(f)
        user_conversation = next((conv for conv in data["conversations"] if conv["user_id"] == user_id), None)
        if user_conversation:
            emails_ordered = [
                OrderedDict([
                    ("from", email["from"]),
                    ("to", email["to"]),
                    ("subject", email["subject"]),
                    ("body", email["body"])
                ])
                for email in user_conversation["emails"]
            ]
            response = OrderedDict([
                ("user_name", user_conversation["user_name"]),
                ("emails", emails_ordered)
            ])
            response_json = json.dumps(response, indent=4)
            return Response(response_json, mimetype='application/json')
        else:
            abort(404)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
